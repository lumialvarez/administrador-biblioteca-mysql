﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_Biblioteca.Modelo
{
    public class Prestamo
    {
        public Prestamo()
        {
            personaPrestador = new Persona();
            persona = new Persona();
            detalle = new List<PrestamoDetalle>();
        }

        public int codPrestamo { get; set; }
        public DateTime fechaPrestamo { get; set; }
        public Persona personaPrestador { get; set; }
        public Persona persona { get; set; }
        public List<PrestamoDetalle> detalle { get; set; }
    }
}
