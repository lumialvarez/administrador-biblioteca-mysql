﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_Biblioteca.Modelo
{
    public class PrestamoDetalle
    {
        public PrestamoDetalle()
        {
            libro = new Libro();
        }

        public Libro libro { get; set; }
        public DateTime fecha_entrega_esperada { get; set; }
        public DateTime fecha_entrega { get; set; }
        public int cantidad { get; set; }
        public string observacion { get; set; }
        public int dias_a_fecha_entrega_esperada { get; set; }
    }
}
