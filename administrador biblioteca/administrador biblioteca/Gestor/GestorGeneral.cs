﻿using Administrador_Biblioteca.Modelo;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_Biblioteca
{
    public class GestorGeneral
    {
        public MySqlConnection conn;
        private GeneralDAO dao;


        public GestorGeneral(MySqlConnection conn)
        {
            this.conn = conn;
            dao = new GeneralDAO(conn);
        }

        public GestorGeneral(string connection)
        {
            conn = new MySqlConnection(connection);
            conn.Open();
            dao = new GeneralDAO(conn);
        }

        public bool probarConexion()
        {
            return dao.probarConexion();
        }

        public DataTable autenticarUsuario(string nomUsuario, string contraUsuario)
        {
            return dao.autenticarUsuario(nomUsuario, contraUsuario);
        }

        public bool actualizarUsuario(string documentoPersona, string nombreUsuario, String contraseña, bool activo, bool admin)
        {
            return dao.actualizarUsuario(documentoPersona, nombreUsuario, contraseña, activo, admin);
        }

        public bool agregarNuevoUsuario(string documentoPersona, string nombreUsuario, String contraseña, bool activo, bool admin)
        {
            return dao.agregarNuevoUsuario(documentoPersona, nombreUsuario, contraseña, activo, admin);
        }

        public void crearTemporalesPrestamo()
        {
            dao.crearTemporalesPrestamo();
        }

        public Prestamo consultarPrestamoTemporal()
        {
            return dao.consultarPrestamoTemporal();
        }

        public bool agregarPersonaPrestamoTemporal(Persona persona, String documentoResponsable)
        {
            return dao.agregarPersonaPrestamoTemporal(persona, documentoResponsable);
        }

        internal bool agregarLibroPrestamoTemporal(PrestamoDetalle detalle)
        {
            return dao.agregarLibroPrestamoTemporal(detalle);
        }

        internal bool eliminarLibroPrestamoTemporal(string codigoLibro)
        {
            return dao.eliminarLibroPrestamoTemporal(codigoLibro);
        }

        internal bool guardarPrestamo()
        {
            return dao.guardarPrestamo();
        }
    }
}
