﻿using Administrador_Biblioteca.DAO;
using Administrador_Biblioteca.Modelo;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_Biblioteca.Gestor
{
    public class GestorPrestamo : GestorGeneral
    {
        private PrestamoDAO dao;
        public GestorPrestamo(MySqlConnection conn)
            : base(conn)
        {
            this.conn = conn;
            this.dao = new PrestamoDAO(conn);
        }

        internal Prestamo cargarInfoPrestamo(int codPrestamo)
        {
            return dao.cargarInfoPrestamo(codPrestamo);
        }

        internal List<Prestamo> cargarPrestamosPendientes()
        {
            return dao.cargarPrestamosPendientes();
        }

        internal bool marcarEntregaPrestamo(int codPrestamo, string codLibro, DateTime fechaEntrega)
        {
            return dao.marcarEntregaPrestamo(codPrestamo, codLibro, fechaEntrega);
        }
    }
}
