﻿namespace Administrador_Biblioteca
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_devolucion = new System.Windows.Forms.Button();
            this.tbn_usuario = new System.Windows.Forms.Button();
            this.btn_lectores = new System.Windows.Forms.Button();
            this.tbn_ayuda = new System.Windows.Forms.Button();
            this.btn_libros = new System.Windows.Forms.Button();
            this.btn_prestamo = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblDatosUsuario = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.btn_devolucion);
            this.groupBox1.Controls.Add(this.tbn_usuario);
            this.groupBox1.Controls.Add(this.btn_lectores);
            this.groupBox1.Controls.Add(this.tbn_ayuda);
            this.groupBox1.Controls.Add(this.btn_libros);
            this.groupBox1.Controls.Add(this.btn_prestamo);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(798, 86);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Opciones principales";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(644, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(148, 67);
            this.panel1.TabIndex = 6;
            // 
            // btn_devolucion
            // 
            this.btn_devolucion.Image = global::Administrador_Biblioteca.Properties.Resources.Return_Book_26;
            this.btn_devolucion.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_devolucion.Location = new System.Drawing.Point(396, 19);
            this.btn_devolucion.Name = "btn_devolucion";
            this.btn_devolucion.Size = new System.Drawing.Size(80, 50);
            this.btn_devolucion.TabIndex = 3;
            this.btn_devolucion.Text = "Devolucion";
            this.btn_devolucion.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_devolucion.UseVisualStyleBackColor = true;
            this.btn_devolucion.Click += new System.EventHandler(this.btn_devolucion_Click);
            // 
            // tbn_usuario
            // 
            this.tbn_usuario.Image = global::Administrador_Biblioteca.Properties.Resources.User_Menu_26;
            this.tbn_usuario.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.tbn_usuario.Location = new System.Drawing.Point(12, 19);
            this.tbn_usuario.Name = "tbn_usuario";
            this.tbn_usuario.Size = new System.Drawing.Size(87, 50);
            this.tbn_usuario.TabIndex = 5;
            this.tbn_usuario.Text = "Conf. Usuario";
            this.tbn_usuario.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tbn_usuario.UseVisualStyleBackColor = true;
            this.tbn_usuario.Click += new System.EventHandler(this.tbn_usuario_Click);
            // 
            // btn_lectores
            // 
            this.btn_lectores.Image = global::Administrador_Biblioteca.Properties.Resources.User_Group_26;
            this.btn_lectores.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_lectores.Location = new System.Drawing.Point(205, 19);
            this.btn_lectores.Name = "btn_lectores";
            this.btn_lectores.Size = new System.Drawing.Size(80, 50);
            this.btn_lectores.TabIndex = 0;
            this.btn_lectores.Text = "Lectores";
            this.btn_lectores.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_lectores.UseVisualStyleBackColor = true;
            this.btn_lectores.Click += new System.EventHandler(this.btn_lectores_Click);
            // 
            // tbn_ayuda
            // 
            this.tbn_ayuda.Image = global::Administrador_Biblioteca.Properties.Resources.Help_26;
            this.tbn_ayuda.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.tbn_ayuda.Location = new System.Drawing.Point(519, 19);
            this.tbn_ayuda.Name = "tbn_ayuda";
            this.tbn_ayuda.Size = new System.Drawing.Size(80, 50);
            this.tbn_ayuda.TabIndex = 4;
            this.tbn_ayuda.Text = "Ayuda";
            this.tbn_ayuda.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tbn_ayuda.UseVisualStyleBackColor = true;
            // 
            // btn_libros
            // 
            this.btn_libros.Image = global::Administrador_Biblioteca.Properties.Resources.Literature_26;
            this.btn_libros.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_libros.Location = new System.Drawing.Point(119, 19);
            this.btn_libros.Name = "btn_libros";
            this.btn_libros.Size = new System.Drawing.Size(80, 50);
            this.btn_libros.TabIndex = 1;
            this.btn_libros.Text = "Libros";
            this.btn_libros.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_libros.UseVisualStyleBackColor = true;
            this.btn_libros.Click += new System.EventHandler(this.btn_libros_Click);
            // 
            // btn_prestamo
            // 
            this.btn_prestamo.Image = global::Administrador_Biblioteca.Properties.Resources.Lend_Book_26;
            this.btn_prestamo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_prestamo.Location = new System.Drawing.Point(310, 19);
            this.btn_prestamo.Name = "btn_prestamo";
            this.btn_prestamo.Size = new System.Drawing.Size(80, 50);
            this.btn_prestamo.TabIndex = 2;
            this.btn_prestamo.Text = "Prestamo";
            this.btn_prestamo.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_prestamo.UseVisualStyleBackColor = true;
            this.btn_prestamo.Click += new System.EventHandler(this.btn_prestamo_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(12, 117);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(798, 274);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Resumen";
            // 
            // lblDatosUsuario
            // 
            this.lblDatosUsuario.AutoSize = true;
            this.lblDatosUsuario.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatosUsuario.Location = new System.Drawing.Point(9, 9);
            this.lblDatosUsuario.Name = "lblDatosUsuario";
            this.lblDatosUsuario.Size = new System.Drawing.Size(75, 13);
            this.lblDatosUsuario.TabIndex = 9;
            this.lblDatosUsuario.Text = "label Usuario";
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 400);
            this.Controls.Add(this.lblDatosUsuario);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administrador de Biblioteca";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPrincipal_FormClosing);
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_lectores;
        private System.Windows.Forms.Button btn_libros;
        private System.Windows.Forms.Button btn_prestamo;
        private System.Windows.Forms.Button btn_devolucion;
        private System.Windows.Forms.Button tbn_ayuda;
        private System.Windows.Forms.Button tbn_usuario;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblDatosUsuario;
    }
}

