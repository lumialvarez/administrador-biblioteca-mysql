﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_Biblioteca.DAO
{
    public class UtilsDAO
    {
        public string sql = "";
        public MySqlConnection conn;

        public int ejecutaDML(string sql)
        {
            DataTable dt = new DataTable();
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            int result = cmd.ExecuteNonQuery();
            return result;
        }

        public DataTable ejecutaConsulta(string sql)
        {
            DataTable dt = new DataTable();
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            var reader = cmd.ExecuteReader();
            try
            {
                dt.Load(reader);
            }
            catch (Exception e)
            {
                System.Console.Write("Error al pasar el reader a mysql " + e.Message);
                throw e;
            }
            finally
            {
                reader.Close();
            }
            return dt;
        }

        public String ejecutaScalar(string sql)
        {
            DataTable dt = new DataTable();
            dt = ejecutaConsulta(sql);
            String result = dt.Rows[0].ToString();
            return result;
        }
    }
}
