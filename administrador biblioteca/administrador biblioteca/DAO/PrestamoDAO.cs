﻿
using Administrador_Biblioteca.Modelo;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_Biblioteca.DAO
{
    class PrestamoDAO : UtilsDAO
    {
        public PrestamoDAO(MySqlConnection conn)
        {
            this.conn = conn;
        }

        internal Prestamo cargarInfoPrestamo(int codPrestamo)
        {
            Prestamo p = new Prestamo();

            DataTable dt = new DataTable();
            sql = "select p.cod_prestamo, p.fecha_prestamo, res.documento_persona documento_persona_prestador, " +
                    "res.nombres nombres_prestador, res.apellidos apellidos_prestador, u.nombre_usuario, " +
                    "per.documento_persona, per.nombres, per.apellidos, per.email, per.telefono " +
                    "from t_prestamo p " +
                    "inner join t_persona res on(p.usuario_prestador = res.documento_persona) " +
                    "left join t_usuario u on(p.usuario_prestador = u.documento_persona) " +
                    "inner join t_persona per on(p.documento_persona = per.documento_persona) " +
                    "where p.cod_prestamo = "+codPrestamo+";";
            dt = ejecutaConsulta(sql);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    p.codPrestamo = Int32.Parse(row["cod_prestamo"].ToString());
                    p.fechaPrestamo = DateTime.Parse(row["fecha_prestamo"].ToString());
                    p.personaPrestador.documento_persona = row["documento_persona_prestador"].ToString();
                    p.personaPrestador.nombres = row["nombres_prestador"].ToString();
                    p.personaPrestador.apellidos = row["apellidos_prestador"].ToString();
                    p.personaPrestador.usuario = row["nombre_usuario"].ToString();

                    p.persona.documento_persona = row["documento_persona"].ToString();
                    p.persona.nombres = row["nombres"].ToString();
                    p.persona.apellidos = row["apellidos"].ToString();
                    p.persona.email = row["email"].ToString();
                    p.persona.telefono = row["telefono"].ToString();
                }
            }
            else
            {
                throw new Exception("No se ecuentra Prestamo Codigo " + codPrestamo);
            }


            sql = "select pd.cantidad, pd.fecha_entrega_esperada, pd.fecha_entrega, pd.observacion, " +
                    "l.cod_libro, l.nombre_libro, l.isbn, e.nombre_editorial, " +
                    "(current_date - fecha_entrega_esperada) dias_a_fecha_entega_esperada " +
                    "from t_prestamo_detalle pd " +
                    "inner join t_libro l on(pd.cod_libro = l.cod_libro) " +
                    "inner join t_editorial e on(l.cod_editorial = e.cod_editorial) " +
                    "where pd.cod_prestamo = "+codPrestamo+";";
            dt = ejecutaConsulta(sql);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    PrestamoDetalle dp = new PrestamoDetalle();

                    dp.cantidad = Int32.Parse(row["cantidad"].ToString());
                    dp.fecha_entrega_esperada = DateTime.Parse(row["fecha_entrega_esperada"].ToString());
                    string strFechaEntregada = row["fecha_entrega"].ToString();
                    if (!strFechaEntregada.Equals(""))
                        dp.fecha_entrega = DateTime.Parse(strFechaEntregada);

                    dp.libro.cod_libro = row["cod_libro"].ToString();
                    dp.libro.nombre_libro = row["nombre_libro"].ToString();
                    dp.libro.isbn = row["isbn"].ToString();
                    dp.libro.editorial = row["nombre_editorial"].ToString();
                    dp.dias_a_fecha_entrega_esperada = Int32.Parse(row["dias_a_fecha_entega_esperada"].ToString());

                    p.detalle.Add(dp);
                }
            }
            else
            {
                throw new Exception("No se ecuentra Detalle del Prestamo Codigo " + codPrestamo);
            }

            return p;
        }

        internal List<Prestamo> cargarPrestamosPendientes()
        {
            List<Prestamo> lista = new List<Prestamo>();
            DataTable dt = new DataTable();

            sql = "select p.cod_prestamo " +
                    "from t_prestamo p " +
                    "inner join t_prestamo_detalle pd on(p.cod_prestamo = pd.cod_prestamo) " +
                    "where pd.fecha_entrega is null;";

            dt = ejecutaConsulta(sql);


            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    lista.Add(cargarInfoPrestamo(Int32.Parse(row["cod_prestamo"].ToString())));
                }
            }

            return lista;
        }


        internal bool marcarEntregaPrestamo(int codPrestamo, string codLibro, DateTime fechaEntrega)
        {
            sql = "update t_prestamo_detalle set fecha_entrega = '" + fechaEntrega.ToString("yyyy-MM-dd") + "' " +
                    "where cod_prestamo = " + codPrestamo + " and cod_libro = " + codLibro + ";";

            int result = ejecutaDML(sql);

            return result > 0;
        }
    }
}
