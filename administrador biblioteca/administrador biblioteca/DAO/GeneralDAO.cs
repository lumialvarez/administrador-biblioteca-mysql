﻿using Administrador_Biblioteca.DAO;
using Administrador_Biblioteca.Gestor;
using Administrador_Biblioteca.Modelo;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_Biblioteca
{
    public class GeneralDAO : UtilsDAO 
    {
        public GeneralDAO(MySqlConnection conn)
        {
            this.conn = conn;
        }

        public bool probarConexion()
        {
            DataTable dt = new DataTable();

            sql = "select 1 conexion;";

            dt = ejecutaConsulta(sql);

            return (dt != null);
        }


        public DataTable  autenticarUsuario(string nomUsuario, string contraUsuario){
            DataTable dt = new DataTable();

            sql = "select documento_persona, super_usuario, nombres, apellidos " +
                    " from t_usuario u " +
                    " inner join t_persona p using(documento_persona)  " +
                    " where nombre_usuario = '" + nomUsuario + "' and upper(contrasena) = upper('" + contraUsuario + "') " +
                    " and p.activo = 'S' and u.activo = 'S';";

            dt = ejecutaConsulta(sql);

            return dt;
        }

        public bool actualizarUsuario(string documentoPersona, string nombreUsuario, String contraseña, bool activo, bool admin)
        {
            int resultado = 0;
            sql = "UPDATE t_usuario " +
                "      SET nombre_usuario='" + nombreUsuario + "', contrasena='" + contraseña + "', activo= '" + (activo ? "S" : "N") + "', super_usuario='" + (admin ? "S" : "N") + "' " +
                  "    WHERE documento_persona= '" + documentoPersona + "';";

            resultado = ejecutaDML(sql);
            if (resultado > 0)
                return true;
            return false;
        }

        public bool agregarNuevoUsuario(string documentoPersona, string nombreUsuario, String contraseña, bool activo, bool admin)
        {
            int resultado = 0;
            sql = "INSERT INTO t_usuario( " +
                   "          documento_persona, nombre_usuario, contrasena, activo, super_usuario) " +
                   "  VALUES ('" + documentoPersona + "', '" + nombreUsuario + "', '" + contraseña + "','" + (activo ? "S" : "N") + "' ,'" + (admin ? "S" : "N") + "' );";
            resultado = ejecutaDML(sql);
            if (resultado > 0)
                return true;
            return false;
        }

        public void crearTemporalesPrestamo()
        {
            sql = "DROP TABLE IF EXISTS tmp_prestamo; " +
                        " CREATE TEMPORARY TABLE tmp_prestamo " +
                        " ( " +
                        "   documento_persona character varying(15), " +
                        "   usuario_prestador character varying(15) " +
                        " );  ";
            ejecutaDML(sql);

            sql = "DROP TABLE IF EXISTS tmp_prestamo_detalle; " +
                        " CREATE TEMPORARY TABLE tmp_prestamo_detalle " +
                        " ( " +
                        "   cod_libro integer, " +
                        "   cantidad integer, " +
                        "   fecha_entrega_esperada date, " +
                        "   observacion text " +
                        " );  ";
            ejecutaDML(sql);
        }

        public Prestamo consultarPrestamoTemporal()
        {
            Prestamo prestamo = new Prestamo();
            DataTable dt = new DataTable();
            sql = "select documento_persona, usuario_prestador from tmp_prestamo;";
            dt = ejecutaConsulta(sql);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    prestamo.personaPrestador.documento_persona = row["usuario_prestador"].ToString();
                    prestamo.persona = (new GestorPersona(conn)).cargarInfoPersona(row["documento_persona"].ToString());
                }
            }

            sql = "select cod_libro, cantidad, fecha_entrega_esperada, observacion, nombre_libro  " +
                    " from tmp_prestamo_detalle  " +
                    " inner join t_libro using(cod_libro);";
            dt = ejecutaConsulta(sql);
            List<PrestamoDetalle> detalle = new List<PrestamoDetalle>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    PrestamoDetalle d = new PrestamoDetalle();
                    d.libro.cod_libro = row["cod_libro"].ToString();
                    d.cantidad = Int32.Parse(row["cantidad"].ToString());
                    d.fecha_entrega_esperada = DateTime.Parse(row["fecha_entrega_esperada"].ToString());
                    d.observacion = row["observacion"].ToString();
                    d.libro.nombre_libro = row["nombre_libro"].ToString();
                    detalle.Add(d);
                }
            }
            prestamo.detalle = detalle;

            return prestamo;
        }


        public bool agregarPersonaPrestamoTemporal(Persona persona, String documentoResponsable)
        {
            int resultado = 0;

            ejecutaDML("begin;");

            sql = "DELETE FROM tmp_prestamo;";
            resultado = ejecutaDML(sql);

            sql = "INSERT INTO tmp_prestamo( " +
                   "          documento_persona, usuario_prestador) " +
                   "  VALUES ('" + persona.documento_persona + "', '" + documentoResponsable + "');";
            resultado = ejecutaDML(sql);
            if (resultado > 0)
            {
                ejecutaDML("commit;");
                return true;
            }
            ejecutaDML("rollback;");
            return false;
        }

        internal bool agregarLibroPrestamoTemporal(PrestamoDetalle detalle)
        {
            int resultado = 0;
            sql = "INSERT INTO tmp_prestamo_detalle( " +
                   "          cod_libro, cantidad, fecha_entrega_esperada, observacion) " +
                   "  VALUES (" + detalle.libro.cod_libro + ", " + detalle.cantidad + ", '" + detalle.fecha_entrega_esperada.ToString("yyyy-MM-dd") + "', '" + detalle.observacion + "');";
            resultado = ejecutaDML(sql);
            if (resultado > 0)
                return true;
            return false;
        }

        internal bool eliminarLibroPrestamoTemporal(string codigoLibro)
        {
            int resultado = 0;
            sql = "DELETE FROM tmp_prestamo_detalle WHERE cod_libro = "+codigoLibro+";";
            resultado = ejecutaDML(sql);
            if (resultado > 0)
                return true;
            return false;
        }

        internal bool guardarPrestamo()
        {
            int resultado = 0;
            ejecutaDML("begin;");
            sql = "INSERT INTO t_prestamo(documento_persona, usuario_prestador) " +
                   "  select documento_persona, usuario_prestador FROM tmp_prestamo;";
            resultado = ejecutaDML(sql);
            if (resultado > 0)
            {
                resultado = 0;
                sql = "INSERT INTO t_prestamo_detalle(cod_prestamo, cod_libro, cantidad, fecha_entrega_esperada, observacion) " +
                       "  select last_insert_id(), cod_libro, cantidad, fecha_entrega_esperada, observacion FROM tmp_prestamo_detalle;";
                resultado = ejecutaDML(sql);
            }

            if (resultado > 0)
            {
                ejecutaDML("commit;");
                return true;
            }
            ejecutaDML("rollback;");
            return false;
        }

        

    }
}
