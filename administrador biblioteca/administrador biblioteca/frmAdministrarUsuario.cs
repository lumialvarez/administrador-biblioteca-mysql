﻿using Administrador_Biblioteca.Gestor;
using Administrador_Biblioteca.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Administrador_Biblioteca
{
    public partial class frmAdministrarUsuario : Form
    {
        private Modelo.Configuracion CONFIG;
        private GestorPersona gestorPersona;
        private Persona persona;
        public frmAdministrarUsuario(Modelo.Configuracion CONFIG, GestorPersona gestorPersona, Persona persona)
        {
            InitializeComponent();
            this.CONFIG = CONFIG;
            this.gestorPersona = gestorPersona;
            this.persona = persona;
        }

        private void frmAdministrarUsuario_Load(object sender, EventArgs e)
        {
            txtNombreUsuario.Text = persona.usuario;
            chbActivo.Checked = persona.usuario_activo;
            chbAdministrador.Checked = persona.usuario_administrador;
            panelAmin.Visible = persona.usuario_administrador;
            Modelo.Persona usuarioActual = gestorPersona.cargarInfoPersona(CONFIG.cod_usuario_actual);
            panelAmin.Visible = usuarioActual.usuario_administrador;
            txtPasswordActual.Enabled = persona.usuario_activo;

            panelAmin.Visible = !persona.documento_persona.Equals(CONFIG.cod_usuario_actual);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (txtNombreUsuario.Text.Trim().Equals(""))
            {
                MessageBox.Show("Ingrese el nombre de Usuario!");
                return;
            }

            if (txtPasswordActual.Enabled && txtPasswordActual.Text.Trim().Equals(""))
            {
                MessageBox.Show("Ingrese la contraseña anterior!");
                return;
            }

            if (txtPassword.Text.Trim().Equals("") || txtPasswordConfir.Text.Trim().Equals(""))
            {
                MessageBox.Show("Ingrese la nueva contraseña!");
                return;
            }

            if (!txtPassword.Text.Trim().Equals(txtPasswordConfir.Text.Trim()))
            {
                MessageBox.Show("La nueva contraseña no coincide!");
                return;
            }

            try
            {
                if (persona.usuario_activo)
                {
                    DataTable dt = gestorPersona.autenticarUsuario(persona.usuario, Utils.CalculateMD5Hash(txtPasswordActual.Text));
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        bool result = gestorPersona.actualizarUsuario(persona.documento_persona, txtNombreUsuario.Text, Utils.CalculateMD5Hash(txtPassword.Text), chbActivo.Checked, chbAdministrador.Checked);
                        if (result)
                        {
                            MessageBox.Show("Usuario Actualizado!!");
                        }
                        else
                        {
                            MessageBox.Show("Error al Actualizar el usuario!!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Usuario Invalido, la contraseña del usuario " + persona.usuario + " es invalida!!");
                    }
                }
                else
                {
                    bool result = gestorPersona.agregarNuevoUsuario(persona.documento_persona, txtNombreUsuario.Text, Utils.CalculateMD5Hash(txtPassword.Text), chbActivo.Checked, chbAdministrador.Checked);
                    if (result)
                    {
                        MessageBox.Show("Usuario Agregado!!");
                    }
                    else
                    {
                        MessageBox.Show("Error al Guardar el nuevo usuario!!");
                    }
                }
                    

            }
            catch (Exception ex)
            {
                
            }
            
        }


    }
}
