﻿namespace Administrador_Biblioteca
{
    partial class frmAdministrarPrestamos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAdministrarPrestamos));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tblListaPrestamosPendientes = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFiltroDocumento = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFiltroNombre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFiltroLibro = new System.Windows.Forms.TextBox();
            this.codPrestamo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecha_prestamo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.documento_persona = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.persona_responsable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cod_libro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombre_libro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombre_editorial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecha_acordada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnActualizar = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Prestamo = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpFechaEntregaFinal = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.lblResponsable = new System.Windows.Forms.Label();
            this.lblCodigoPrestamo = new System.Windows.Forms.Label();
            this.lblFechaPrestamo = new System.Windows.Forms.Label();
            this.lblLibro = new System.Windows.Forms.Label();
            this.lblFechaAcordada = new System.Windows.Forms.Label();
            this.lblUsuarioPrestador = new System.Windows.Forms.Label();
            this.btnGuardarEntrega = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblListaPrestamosPendientes)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.tblListaPrestamosPendientes);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1105, 336);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Prestamos";
            // 
            // tblListaPrestamosPendientes
            // 
            this.tblListaPrestamosPendientes.AllowUserToAddRows = false;
            this.tblListaPrestamosPendientes.AllowUserToDeleteRows = false;
            this.tblListaPrestamosPendientes.AllowUserToResizeRows = false;
            this.tblListaPrestamosPendientes.BackgroundColor = System.Drawing.SystemColors.Control;
            this.tblListaPrestamosPendientes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.tblListaPrestamosPendientes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.tblListaPrestamosPendientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tblListaPrestamosPendientes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codPrestamo,
            this.fecha_prestamo,
            this.documento_persona,
            this.persona_responsable,
            this.cod_libro,
            this.nombre_libro,
            this.nombre_editorial,
            this.fecha_acordada,
            this.estado});
            this.tblListaPrestamosPendientes.EnableHeadersVisualStyles = false;
            this.tblListaPrestamosPendientes.GridColor = System.Drawing.SystemColors.Control;
            this.tblListaPrestamosPendientes.Location = new System.Drawing.Point(6, 73);
            this.tblListaPrestamosPendientes.MultiSelect = false;
            this.tblListaPrestamosPendientes.Name = "tblListaPrestamosPendientes";
            this.tblListaPrestamosPendientes.ReadOnly = true;
            this.tblListaPrestamosPendientes.RowHeadersVisible = false;
            this.tblListaPrestamosPendientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tblListaPrestamosPendientes.Size = new System.Drawing.Size(1093, 257);
            this.tblListaPrestamosPendientes.TabIndex = 1;
            this.tblListaPrestamosPendientes.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tblListaPrestamosPendientes_CellClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnActualizar);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtFiltroLibro);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtFiltroNombre);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtFiltroDocumento);
            this.groupBox2.Location = new System.Drawing.Point(6, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1093, 48);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filtros";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Documento:";
            // 
            // txtFiltroDocumento
            // 
            this.txtFiltroDocumento.Location = new System.Drawing.Point(74, 22);
            this.txtFiltroDocumento.Name = "txtFiltroDocumento";
            this.txtFiltroDocumento.Size = new System.Drawing.Size(130, 20);
            this.txtFiltroDocumento.TabIndex = 2;
            this.txtFiltroDocumento.TextChanged += new System.EventHandler(this.txtFiltroDocumento_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(245, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nombre:";
            // 
            // txtFiltroNombre
            // 
            this.txtFiltroNombre.Location = new System.Drawing.Point(298, 22);
            this.txtFiltroNombre.Name = "txtFiltroNombre";
            this.txtFiltroNombre.Size = new System.Drawing.Size(130, 20);
            this.txtFiltroNombre.TabIndex = 4;
            this.txtFiltroNombre.TextChanged += new System.EventHandler(this.txtFiltroNombre_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(466, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Libro:";
            // 
            // txtFiltroLibro
            // 
            this.txtFiltroLibro.Location = new System.Drawing.Point(505, 22);
            this.txtFiltroLibro.Name = "txtFiltroLibro";
            this.txtFiltroLibro.Size = new System.Drawing.Size(130, 20);
            this.txtFiltroLibro.TabIndex = 6;
            this.txtFiltroLibro.TextChanged += new System.EventHandler(this.txtFiltroLibro_TextChanged);
            // 
            // codPrestamo
            // 
            this.codPrestamo.HeaderText = "Codigo";
            this.codPrestamo.Name = "codPrestamo";
            this.codPrestamo.ReadOnly = true;
            this.codPrestamo.Width = 60;
            // 
            // fecha_prestamo
            // 
            this.fecha_prestamo.HeaderText = "Fecha Prestamo";
            this.fecha_prestamo.Name = "fecha_prestamo";
            this.fecha_prestamo.ReadOnly = true;
            this.fecha_prestamo.Width = 135;
            // 
            // documento_persona
            // 
            this.documento_persona.HeaderText = "Documento";
            this.documento_persona.Name = "documento_persona";
            this.documento_persona.ReadOnly = true;
            this.documento_persona.Width = 90;
            // 
            // persona_responsable
            // 
            this.persona_responsable.HeaderText = "Responsable";
            this.persona_responsable.Name = "persona_responsable";
            this.persona_responsable.ReadOnly = true;
            this.persona_responsable.Width = 160;
            // 
            // cod_libro
            // 
            this.cod_libro.HeaderText = "Cod Libro";
            this.cod_libro.Name = "cod_libro";
            this.cod_libro.ReadOnly = true;
            this.cod_libro.Width = 80;
            // 
            // nombre_libro
            // 
            this.nombre_libro.HeaderText = "Nombre";
            this.nombre_libro.Name = "nombre_libro";
            this.nombre_libro.ReadOnly = true;
            this.nombre_libro.Width = 190;
            // 
            // nombre_editorial
            // 
            this.nombre_editorial.HeaderText = "Editorial";
            this.nombre_editorial.Name = "nombre_editorial";
            this.nombre_editorial.ReadOnly = true;
            this.nombre_editorial.Width = 150;
            // 
            // fecha_acordada
            // 
            this.fecha_acordada.HeaderText = "Fecha Entrega Acordada";
            this.fecha_acordada.Name = "fecha_acordada";
            this.fecha_acordada.ReadOnly = true;
            this.fecha_acordada.Width = 110;
            // 
            // estado
            // 
            this.estado.HeaderText = "Estado";
            this.estado.Name = "estado";
            this.estado.ReadOnly = true;
            // 
            // btnActualizar
            // 
            this.btnActualizar.Location = new System.Drawing.Point(999, 15);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(88, 23);
            this.btnActualizar.TabIndex = 7;
            this.btnActualizar.Text = "Actualizar";
            this.btnActualizar.UseVisualStyleBackColor = true;
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnGuardarEntrega);
            this.groupBox3.Controls.Add(this.lblUsuarioPrestador);
            this.groupBox3.Controls.Add(this.lblFechaAcordada);
            this.groupBox3.Controls.Add(this.lblLibro);
            this.groupBox3.Controls.Add(this.lblFechaPrestamo);
            this.groupBox3.Controls.Add(this.lblCodigoPrestamo);
            this.groupBox3.Controls.Add(this.lblResponsable);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.dtpFechaEntregaFinal);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.Prestamo);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Location = new System.Drawing.Point(12, 354);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1104, 144);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Detalle prestamo";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(64, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Codigo Prestamo:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(67, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Fecha Prestamo:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(77, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(157, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Usuario que realizó el prestamo:";
            // 
            // Prestamo
            // 
            this.Prestamo.AutoSize = true;
            this.Prestamo.Location = new System.Drawing.Point(12, 22);
            this.Prestamo.Name = "Prestamo";
            this.Prestamo.Size = new System.Drawing.Size(142, 13);
            this.Prestamo.TabIndex = 3;
            this.Prestamo.Text = "Reponsable por el prestamo:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(544, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Libro:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(433, 45);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(144, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Fecha de Entrega Acordada:";
            // 
            // dtpFechaEntregaFinal
            // 
            this.dtpFechaEntregaFinal.Location = new System.Drawing.Point(583, 61);
            this.dtpFechaEntregaFinal.Name = "dtpFechaEntregaFinal";
            this.dtpFechaEntregaFinal.Size = new System.Drawing.Size(198, 20);
            this.dtpFechaEntregaFinal.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(457, 67);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Fecha de Entrega Final:";
            // 
            // lblResponsable
            // 
            this.lblResponsable.AutoSize = true;
            this.lblResponsable.Location = new System.Drawing.Point(156, 22);
            this.lblResponsable.Name = "lblResponsable";
            this.lblResponsable.Size = new System.Drawing.Size(10, 13);
            this.lblResponsable.TabIndex = 8;
            this.lblResponsable.Text = "-";
            // 
            // lblCodigoPrestamo
            // 
            this.lblCodigoPrestamo.AutoSize = true;
            this.lblCodigoPrestamo.Location = new System.Drawing.Point(156, 44);
            this.lblCodigoPrestamo.Name = "lblCodigoPrestamo";
            this.lblCodigoPrestamo.Size = new System.Drawing.Size(10, 13);
            this.lblCodigoPrestamo.TabIndex = 9;
            this.lblCodigoPrestamo.Text = "-";
            // 
            // lblFechaPrestamo
            // 
            this.lblFechaPrestamo.AutoSize = true;
            this.lblFechaPrestamo.Location = new System.Drawing.Point(156, 67);
            this.lblFechaPrestamo.Name = "lblFechaPrestamo";
            this.lblFechaPrestamo.Size = new System.Drawing.Size(10, 13);
            this.lblFechaPrestamo.TabIndex = 10;
            this.lblFechaPrestamo.Text = "-";
            // 
            // lblLibro
            // 
            this.lblLibro.AutoSize = true;
            this.lblLibro.Location = new System.Drawing.Point(580, 23);
            this.lblLibro.Name = "lblLibro";
            this.lblLibro.Size = new System.Drawing.Size(10, 13);
            this.lblLibro.TabIndex = 11;
            this.lblLibro.Text = "-";
            // 
            // lblFechaAcordada
            // 
            this.lblFechaAcordada.AutoSize = true;
            this.lblFechaAcordada.Location = new System.Drawing.Point(580, 45);
            this.lblFechaAcordada.Name = "lblFechaAcordada";
            this.lblFechaAcordada.Size = new System.Drawing.Size(10, 13);
            this.lblFechaAcordada.TabIndex = 12;
            this.lblFechaAcordada.Text = "-";
            // 
            // lblUsuarioPrestador
            // 
            this.lblUsuarioPrestador.AutoSize = true;
            this.lblUsuarioPrestador.Location = new System.Drawing.Point(240, 112);
            this.lblUsuarioPrestador.Name = "lblUsuarioPrestador";
            this.lblUsuarioPrestador.Size = new System.Drawing.Size(10, 13);
            this.lblUsuarioPrestador.TabIndex = 13;
            this.lblUsuarioPrestador.Text = "-";
            // 
            // btnGuardarEntrega
            // 
            this.btnGuardarEntrega.Location = new System.Drawing.Point(583, 102);
            this.btnGuardarEntrega.Name = "btnGuardarEntrega";
            this.btnGuardarEntrega.Size = new System.Drawing.Size(135, 23);
            this.btnGuardarEntrega.TabIndex = 14;
            this.btnGuardarEntrega.Text = "Guardar Entrega";
            this.btnGuardarEntrega.UseVisualStyleBackColor = true;
            this.btnGuardarEntrega.Click += new System.EventHandler(this.btnGuardarEntrega_Click);
            // 
            // frmAdministrarPrestamos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1129, 541);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAdministrarPrestamos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administrar Prestamos Pendientes";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblListaPrestamosPendientes)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView tblListaPrestamosPendientes;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFiltroLibro;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFiltroNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFiltroDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn codPrestamo;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecha_prestamo;
        private System.Windows.Forms.DataGridViewTextBoxColumn documento_persona;
        private System.Windows.Forms.DataGridViewTextBoxColumn persona_responsable;
        private System.Windows.Forms.DataGridViewTextBoxColumn cod_libro;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre_libro;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre_editorial;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecha_acordada;
        private System.Windows.Forms.DataGridViewTextBoxColumn estado;
        private System.Windows.Forms.Button btnActualizar;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label Prestamo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtpFechaEntregaFinal;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblResponsable;
        private System.Windows.Forms.Label lblUsuarioPrestador;
        private System.Windows.Forms.Label lblFechaAcordada;
        private System.Windows.Forms.Label lblLibro;
        private System.Windows.Forms.Label lblFechaPrestamo;
        private System.Windows.Forms.Label lblCodigoPrestamo;
        private System.Windows.Forms.Button btnGuardarEntrega;

    }
}