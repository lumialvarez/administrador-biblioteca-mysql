﻿using Administrador_Biblioteca.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Administrador_Biblioteca
{
    public partial class frmAdministrarPrestamos : Form
    {
        private Modelo.Configuracion CONFIG;
        private Gestor.GestorPrestamo gestorPrestamo;
        private List<Prestamo> listaPrestamosPendientes;
        private Prestamo prestamoSeleccionado = new Prestamo();
        private PrestamoDetalle prestamoDetalleSeleccionado = new PrestamoDetalle();

        public frmAdministrarPrestamos()
        {
            InitializeComponent();
        }

        public frmAdministrarPrestamos(Modelo.Configuracion CONFIG, Gestor.GestorPrestamo gestorPrestamo)
        {
            InitializeComponent();
            this.CONFIG = CONFIG;
            this.gestorPrestamo = gestorPrestamo;
            cargarPrestamosPendientes();
            cargarFormulario();
        }

        public void cargarPrestamosPendientes()
        {
            try
            {
                listaPrestamosPendientes = gestorPrestamo.cargarPrestamosPendientes();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }

        public void cargarFormulario()
        {
            try
            {
                tblListaPrestamosPendientes.Rows.Clear();
                foreach(Prestamo p in listaPrestamosPendientes){
                    foreach(PrestamoDetalle pd in p.detalle){
                        string nombrePersona = p.persona.nombres + " " + p.persona.apellidos;
                        string estadoPrestamo = "";
                        if (pd.dias_a_fecha_entrega_esperada > 0)
                        {
                            estadoPrestamo = pd.dias_a_fecha_entrega_esperada + " Dias retraso";
                        }
                        else if (pd.dias_a_fecha_entrega_esperada == 0)
                        {
                            estadoPrestamo = "Vence Hoy";
                        }
                        else 
                        {
                            estadoPrestamo = "ok (restan " + Math.Abs(pd.dias_a_fecha_entrega_esperada) + " dias)";
                        }

                        bool filtroDocumento = p.persona.documento_persona.Contains(txtFiltroDocumento.Text.Trim().ToUpper());
                        bool filtroNombre = nombrePersona.Contains(txtFiltroNombre.Text.Trim().ToUpper());
                        bool filtroLibro = pd.libro.nombre_libro.Contains(txtFiltroLibro.Text.Trim().ToUpper());

                        if (filtroDocumento && filtroNombre && filtroLibro)
                        {
                            tblListaPrestamosPendientes.Rows.Add(new object[] {
                            p.codPrestamo,
                            p.fechaPrestamo,
                            p.persona.documento_persona,
                            nombrePersona,
                            pd.libro.cod_libro,
                            pd.libro.nombre_libro,
                            pd.libro.editorial,
                            pd.fecha_entrega_esperada.ToShortDateString(),
                            estadoPrestamo
                            });
                        }
                    }
                }
                tblListaPrestamosPendientes.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }

        private void txtFiltroDocumento_TextChanged(object sender, EventArgs e)
        {
            cargarFormulario();
        }

        private void txtFiltroNombre_TextChanged(object sender, EventArgs e)
        {
            cargarFormulario();
        }

        private void txtFiltroLibro_TextChanged(object sender, EventArgs e)
        {
            cargarFormulario();
        }

        private void tblListaPrestamosPendientes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try {
                int pos = e.RowIndex;
                if (pos > 0)
                {
                    int codPrestamoSeleccionado = Int32.Parse(tblListaPrestamosPendientes.Rows[pos].Cells["codPrestamo"].Value.ToString());
                    string codLibroSeleccionado = tblListaPrestamosPendientes.Rows[pos].Cells["cod_libro"].Value.ToString();

                    foreach (Prestamo p in listaPrestamosPendientes)
                    {
                        if (p.codPrestamo == codPrestamoSeleccionado)
                        {
                            foreach (PrestamoDetalle pd in p.detalle)
                            {
                                if (pd.libro.cod_libro.Equals(codLibroSeleccionado))
                                {
                                    prestamoSeleccionado = p;
                                    prestamoDetalleSeleccionado = pd;
                                    cargarInfoPrestamoFormulario();
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            cargarPrestamosPendientes();
            cargarFormulario();
        }

        private void cargarInfoPrestamoFormulario()
        {
            lblResponsable.Text = prestamoSeleccionado.persona.nombres + " " + prestamoSeleccionado.persona.apellidos + " (" + prestamoSeleccionado.persona.documento_persona + ")";
            lblCodigoPrestamo.Text = prestamoSeleccionado.codPrestamo + "";
            lblFechaPrestamo.Text = prestamoSeleccionado.fechaPrestamo.ToLongDateString() + ", " + prestamoSeleccionado.fechaPrestamo.ToLongTimeString();

            lblLibro.Text = prestamoDetalleSeleccionado.libro.cod_libro + " - " + prestamoDetalleSeleccionado.libro.nombre_libro + ", " + prestamoDetalleSeleccionado.libro.editorial + "(ISBN: " + prestamoDetalleSeleccionado.libro.isbn + ")";
            lblFechaAcordada.Text = prestamoDetalleSeleccionado.fecha_entrega_esperada.ToLongDateString();

            lblUsuarioPrestador.Text = prestamoSeleccionado.personaPrestador.usuario + " (" + prestamoSeleccionado.personaPrestador.nombres + " " + prestamoSeleccionado.personaPrestador.apellidos + ")";
        }

        private void limpiarInfoPrestamoFormulario()
        {
            lblResponsable.Text = "-";
            lblCodigoPrestamo.Text = "-";
            lblFechaPrestamo.Text = "-";

            lblLibro.Text = "-";
            lblFechaAcordada.Text = "-";

            lblUsuarioPrestador.Text = "-";
        }

        private void btnGuardarEntrega_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dr = MessageBox.Show("Esta seguro de marcar como entregado el libro " + prestamoDetalleSeleccionado.libro.nombre_libro + "?", "Confirmar", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                if (dr == DialogResult.OK)
                {
                    bool result = gestorPrestamo.marcarEntregaPrestamo(prestamoSeleccionado.codPrestamo, prestamoDetalleSeleccionado.libro.cod_libro, dtpFechaEntregaFinal.Value);
                    if (result)
                    {
                        MessageBox.Show("Actualización correcta!");
                        cargarPrestamosPendientes();
                        cargarFormulario();
                        limpiarInfoPrestamoFormulario();
                    }
                    else
                    {
                        MessageBox.Show("No Actualizado");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}
