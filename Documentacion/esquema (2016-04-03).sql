﻿CREATE TABLE "t_persona" (
	"documento_persona" varchar(15) NOT NULL,
	"nombres" varchar(40) NOT NULL,
	"apellidos" varchar(40) NOT NULL,
	"telefono" varchar(15),
	"email" varchar(60),
	"activo" varchar(1) NOT NULL,
	CONSTRAINT t_persona_pk PRIMARY KEY ("documento_persona")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "t_usuario" (
	"documento_persona" varchar(15) NOT NULL,
	"nombre_usuario" varchar(20) NOT NULL UNIQUE,
	"contasena" varchar(20) NOT NULL,
	"activo" varchar(1) NOT NULL,
	"super_usuario" varchar(1) NOT NULL,
	CONSTRAINT t_usuario_pk PRIMARY KEY ("documento_persona")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "t_prestamo" (
	"cod_prestamo" integer NOT NULL,
	"documento_persona" varchar(15) NOT NULL,
	CONSTRAINT t_prestamo_pk PRIMARY KEY ("cod_prestamo")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "t_libro" (
	"cod_libro" integer NOT NULL,
	"nombre_libro" varchar NOT NULL,
	"isbn" varchar NOT NULL,
	CONSTRAINT t_libro_pk PRIMARY KEY ("cod_libro")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "t_prestamo_detalle" (
	"cod_prestamo" integer NOT NULL,
	"cod_libro" integer NOT NULL,
	"cantidad" integer NOT NULL,
	"fecha_prestamo" DATE NOT NULL,
	"fecha_entrega_esperada" DATE NOT NULL,
	"fecha_entrega" DATE NOT NULL,
	"observacion" TEXT,
	CONSTRAINT t_prestamo_detalle_pk PRIMARY KEY ("cod_prestamo","cod_libro")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "t_autor" (
	"cod_autor" integer NOT NULL,
	"nombre_autor" varchar(45) NOT NULL,
	"apellido_autor" varchar(45) NOT NULL,
	CONSTRAINT t_autor_pk PRIMARY KEY ("cod_autor")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "t_editorial" (
	"cod_editorial" integer NOT NULL,
	"nombre_editorial" varchar(45) NOT NULL,
	CONSTRAINT t_editorial_pk PRIMARY KEY ("cod_editorial")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "t_categoria" (
	"cod_categoria" integer NOT NULL,
	"nombre_categoria" varchar(45) NOT NULL,
	CONSTRAINT t_categoria_pk PRIMARY KEY ("cod_categoria")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "t_rel_libro_autor" (
	"cod_libro" integer NOT NULL,
	"cod_autor" integer NOT NULL,
	CONSTRAINT t_rel_libro_autor_pk PRIMARY KEY ("cod_libro","cod_autor")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "t_rel_libro_editorial" (
	"cod_libro" integer NOT NULL,
	"cod_editorial" integer NOT NULL,
	CONSTRAINT t_rel_libro_editorial_pk PRIMARY KEY ("cod_libro","cod_editorial")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "t_rel_libro_categoria" (
	"cod_libro" integer NOT NULL,
	"cod_categoria" integer NOT NULL,
	CONSTRAINT t_rel_libro_categoria_pk PRIMARY KEY ("cod_libro","cod_categoria")
) WITH (
  OIDS=FALSE
);




ALTER TABLE "t_usuario" ADD CONSTRAINT "t_usuario_fk0" FOREIGN KEY ("documento_persona") REFERENCES "t_persona"("documento_persona");

ALTER TABLE "t_prestamo" ADD CONSTRAINT "t_prestamo_fk0" FOREIGN KEY ("documento_persona") REFERENCES "t_persona"("documento_persona");


ALTER TABLE "t_prestamo_detalle" ADD CONSTRAINT "t_prestamo_detalle_fk0" FOREIGN KEY ("cod_prestamo") REFERENCES "t_prestamo"("cod_prestamo");
ALTER TABLE "t_prestamo_detalle" ADD CONSTRAINT "t_prestamo_detalle_fk1" FOREIGN KEY ("cod_libro") REFERENCES "t_libro"("cod_libro");




ALTER TABLE "t_rel_libro_autor" ADD CONSTRAINT "t_rel_libro_autor_fk0" FOREIGN KEY ("cod_libro") REFERENCES "t_libro"("cod_libro");
ALTER TABLE "t_rel_libro_autor" ADD CONSTRAINT "t_rel_libro_autor_fk1" FOREIGN KEY ("cod_autor") REFERENCES "t_autor"("cod_autor");

ALTER TABLE "t_rel_libro_editorial" ADD CONSTRAINT "t_rel_libro_editorial_fk0" FOREIGN KEY ("cod_libro") REFERENCES "t_libro"("cod_libro");
ALTER TABLE "t_rel_libro_editorial" ADD CONSTRAINT "t_rel_libro_editorial_fk1" FOREIGN KEY ("cod_editorial") REFERENCES "t_editorial"("cod_editorial");

ALTER TABLE "t_rel_libro_categoria" ADD CONSTRAINT "t_rel_libro_categoria_fk0" FOREIGN KEY ("cod_libro") REFERENCES "t_libro"("cod_libro");
ALTER TABLE "t_rel_libro_categoria" ADD CONSTRAINT "t_rel_libro_categoria_fk1" FOREIGN KEY ("cod_categoria") REFERENCES "t_categoria"("cod_categoria");

